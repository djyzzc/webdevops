import "./App.css";

function App() {
  const elements = [
    { title: "HAPPY", content: "永远相信" },
    { title: "NEW", content: "美好的事情" },
    { title: "YEAR", content: "即将发生" },
    { title: "2022", content: "GOOD LUCK" },
  ];
  return (
    <div className="container">
      {elements.map(element => (
        <div>
          <span>{ element.title }</span>
          <span>{ element.content }</span>
        </div>
      ))}
    </div>
  );
}

export default App;
